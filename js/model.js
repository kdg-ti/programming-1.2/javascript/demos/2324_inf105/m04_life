const HEIGHT=20;
const WIDTH=30;
const PROBABILITY=1.0/3;

function createGrid(height,width,probability) {
  let grid = [];
  for (let y = 0; y < height; y++) {
    grid[y] = [];
    for (let x = 0; x < width; x++) {
      grid[y][x] = Math.random() < probability;
    }
  }
  return grid;
}

export let grid = createGrid(HEIGHT,WIDTH,PROBABILITY);
let next = createGrid(HEIGHT,WIDTH,0);

