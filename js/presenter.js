import {grid} from "./model.js";
const PREFIX="checkbox"
const game = document.getElementById("game");

init();

function createCheckbox(y,x) {
  const checkbox = document.createElement("input");
  checkbox.type = "checkbox";
  checkbox.id=PREFIX+y+x;
  return checkbox;
}

function drawGame(grid) {
  game.replaceChildren();
  for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[y].length; x++) {
      game.append(createCheckbox(y, x));
    }
    game.append(document.createElement("br"))
  }

}

function getCellId(y, x) {
  return PREFIX + y + x
}

function getCell(y, x) {
  return document.getElementById(getCellId(y, x))
}

function updateView(grid) {
  for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[y].length; x++) {
      getCell(y, x).checked=grid[y][x];
    }
  }
}

function init() {
  drawGame(grid);
  updateView(grid);
}